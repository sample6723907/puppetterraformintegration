provider "google" {
  credentials = <<EOT
  {
    "type": "service_account",
    "project_id": "gcp-01-413e3280fd95",
    "private_key_id": "1b5aab044349535b438ad7a301154c324c12920e",
    "private_key": "-----BEGIN PRIVATE KEY-----\n1b5aab044349535b438ad7a301154c324c12920e\n-----END PRIVATE KEY-----\n",
    "client_email": "lehkaleha289@your-gcp-01-413e3280fd95.iam.gserviceaccount.com",
    "client_id": "106617621214494358350",
    "auth_uri": "https://accounts.google.com/o/oauth2/auth",
    "token_uri": "https://accounts.google.com/o/oauth2/token",
    "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
    "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/lehkaleha289%40gcp-01-413e3280fd95.iam.gserviceaccount.com"
  }
  EOT
  project = "gcp-01-413e3280fd95"
  region  = "us-central1"  
}

resource "google_compute_instance" "puppetagent" {
  name         = "puppetagent"
  machine_type = "e2-micro"  # 
  zone         = "us-central1-a"  

  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-20.04-lts"
    }
  }

  network_interface {
    network = "default"
  }

  metadata_startup_script = <<-EOF
    #!/bin/bash
    sudo apt update
    sudo wget https://apt.puppetlabs.com/puppet8-release-bionic.deb
    sudo dpkg -i puppet8-release-bionic.deb
    sudo apt update
    sudo apt install puppet-agent -y
    sudo sh -c 'echo "PUPPET_SERVER_IP puppet" >> /etc/hosts'
    sudo systemctl enable puppet
    sudo systemctl start puppet
    sudo /opt/puppetlabs/bin/puppet agent --test
  EOF
}

output "public_ip" {
  value = google_compute_instance.puppetagent.network_interface.0.access_config.0.assigned_nat_ip
}
