# PuppetTerraformIntegration

The provided procedure outlines the integration of Puppet and Terraform for automating the setup of a Puppet Agent on a Google Cloud Platform (GCP) virtual machine instance.

## Prerequisites:

- Terraform: Ensure that Terraform is installed on your local machine.
- Puppet Master: Have a Puppet Master running, either on a VM or in a cloud provider like GCP.
- Google Cloud Platform (GCP): Make sure you have a GCP account and the necessary credentials configured.

## Procedure

- Clone the repository or fork it and clone it locally.

- Execute the following commands in the terminal:
    - cd ./terraform
    - terraform init
    - terraform validate
    - terraform plan
    - terraform apply

- Place the init.pp file under the manifest directory of the Puppet Server.

- Sign the Puppet Agent certificates from the Puppet Master instance.

- The Terraform script will create a new virtual machine instance on Google Cloud Platform (GCP) and automate the setup of Puppet Agent on it. Once the installation is complete, the Puppet Agent service will be initiated, and it will apply the catalog from the Puppet Server.
